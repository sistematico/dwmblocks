//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
    //{"", "~/.dwmblocks/mpd",				2,		27},
    //{"", "~/.dwmblocks/crypto",			15,		26},
    {"", "~/.dwmblocks/downloads",		15, 	25},
    {"", "~/.dwmblocks/processor", 		3, 		24},
    {"", "~/.dwmblocks/memory",				1, 		23},
    {"", "~/.dwmblocks/disk",					600, 	22},
    {"", "~/.dwmblocks/temperature", 	30, 	21},
    {"", "~/.dwmblocks/volume",				0, 		20},
    {"", "~/.dwmblocks/clock",				60, 	19},
    {"", "~/.dwmblocks/conky",				0,		18},
    //{"", "~/.dwmblocks/theme",			0, 		21},
    {"", "~/.dwmblocks/wallpaper", 		0, 		17},
    //{"", "~/.dwmblocks/code",				0, 		19},
    {"", "~/.dwmblocks/power",				0, 		16}
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 5;
